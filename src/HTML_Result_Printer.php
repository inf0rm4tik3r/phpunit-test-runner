<?php

namespace Lukaspotthast\PHPUnitTestRunner;

use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestResult;
use PHPUnit\Framework\TestSuite;
use PHPUnit\Runner\Version;
use PHPUnit\TextUI\ResultPrinter;
use PHPUnit\Framework\TestFailure;
use PHP_Timer;

/**
 * Class HTML_Result_Printer
 * @package Lukaspotthast\PHPUnitTestRunner
 */
class HTML_Result_Printer extends ResultPrinter
{

    /** @var string */
    private $test_description = '';

    /** @var string */
    private $test_result = '';

    /** @var array  */
    private $test_results = [];

    public function __construct($out = null, bool $verbose = false, string $colors = self::COLOR_DEFAULT, bool $debug = false, $numberOfColumns = 80, bool $reverse = false)
    {
        parent::__construct($out, $verbose, $colors, $debug, $numberOfColumns, $reverse);
    }

    public function write($buffer)
    {
        echo nl2br($buffer);
    }

    public function startTestSuite(TestSuite $suite)
    {
        parent::startTestSuite($suite);
    }

    public function printResult(TestResult $result)
    {
        //parent::printResult($result);

        $this->printHeader();
        $this->printTestResults();
        $this->printErrors($result);
        $this->printWarnings($result);
        $this->printFailures($result);

        if ($this->verbose) {
            $this->printRisky($result);
            $this->printIncompletes($result);
            $this->printSkipped($result);
        }

        $this->printFooter($result);
    }

    public function startTest(Test $test)
    {
        $this->test_description = \PHPUnit\Util\Test::describe($test);
    }

    protected function writeProgress($progress)
    {
        ob_start();
        $this->print_test_case_status($progress);
        parent::writeProgress($progress);
        $result = ob_get_clean();

        $this->test_result = $result;

        array_push($this->test_results, [
            'description' => $this->test_description,
            'result' => $this->test_result
        ]);
    }

    public function endTest(Test $test, $time)
    {
        parent::endTest($test, $time);
    }

    public function endTestSuite(TestSuite $suite)
    {
        parent::endTestSuite($suite);
    }

    /**
     * @param string $buffer Result of the Test Case => . F S I R
     */
    private function print_test_case_status($buffer)
    {
        switch (strtoupper($buffer)) {
            case '.':
                $color = 'fg-green,bold';
                $buffer = mb_convert_encoding("\x27\x13", 'UTF-8', 'UTF-16BE');
                $buffer .= (!$this->debug) ? '' : ' Passed';
                break;
            case 'S':
                $color = 'fg-yellow,bold';
                $buffer = mb_convert_encoding("\x27\xA6", 'UTF-8', 'UTF-16BE');
                $buffer .= (!$this->debug) ? '' : ' Skipped';
                break;
            case 'I':
                $color = 'fg-blue,bold';
                $buffer = 'ℹ';
                $buffer .= (!$this->debug) ? '' : ' Incomplete';
                break;
            case 'F':
                $color = 'fg-red,bold';
                $buffer = mb_convert_encoding("\x27\x16", 'UTF-8', 'UTF-16BE');
                $buffer .= (!$this->debug) ? '' : ' Fail';
                break;
            case 'E':
                $color = 'fg-red,bold';
                $buffer = '⚈';
                $buffer .= (!$this->debug) ? '' : ' Error';
                break;
        }
        $buffer .= ' ';
        $this->write($buffer);
    }

    protected function printHeader()
    {
        $this->write('<div class="phpunit-header">');

        $this->write('<div class="phpunit-version-info">');
        $this->write(Version::getVersionString());
        $this->write('</div>');

        $this->write('<div class="phpunit-resource-usage">');
        //parent::printHeader();
        $this->write(PHP_Timer::resourceUsage());
        $this->write('</div>');

        $this->write('</div>');
    }

    protected function printTestResults(): void
    {
        $this->write('<div class="phpunit-test-results-table-wrapper">');
        $this->write('<table class="phpunit-test-results-table">');
        foreach ( $this->test_results as &$result )
        {
            $this->write('<tr class="phpunit-test-result">');
            {
                $this->write('<td class="phpunit-test-func">');
                $this->write($result['description']);
                $this->write('</td>');

                $this->write('<td class="phpunit-test-func-result">');
                $this->write($result['result']);
                $this->write('</td>');
            }
            $this->write("</tr>");
        }
        $this->write('</table>');
        $this->write('</div>');
    }

    protected function printWarnings(TestResult $result)
    {
        $warning_count = $result->warningCount();
        if ( $warning_count > 0 )
        {
            $this->write('<div class="phpunit-warning">');
        }
        parent::printWarnings($result);
        if ( $warning_count > 0 )
        {
            $this->write('</div>');
        }
    }

    protected function printErrors(TestResult $result)
    {
        if ( $result->errorCount() > 0 )
        {
            $this->write('<div class="phpunit-error">');
            parent::printErrors($result);
            $this->write('</div>');
        }
    }

    protected function printSkipped(TestResult $result)
    {
        if ( $result->skippedCount() > 0 )
        {
            $this->write('<div class="phpunit-skipped">');
            parent::printSkipped($result);
            $this->write('</div>');
        }
    }

    protected function printRisky(TestResult $result)
    {
        if ( $result->riskyCount() > 0 )
        {
            $this->write('<div class="phpunit-risky">');
            parent::printRisky($result);
            $this->write('</div>');
        }
    }

    protected function printIncompletes(TestResult $result)
    {
        if ( count($result->notImplemented()) > 0 )
        {
            $this->write('<div class="phpunit-incomplete">');
            parent::printIncompletes($result);
            $this->write('</div>');
        }
    }

    protected function printFailures(TestResult $result)
    {
        if ( $result->failureCount() > 0 )
        {
            $this->write('<div class="phpunit-failures">');
            parent::printFailures($result);
            $this->write('</div>');
        }
    }

    protected function printDefects(array $defects, $type)
    {
        $defect_count = count($defects);
        if ( $defect_count > 0 )
        {
            $this->write('<div class="phpunit-defects">');

            //................ parent::printDefects() ........
            $this->write('<div class="phpunit-info">');
            $this->write(
                sprintf(
                    "There %s %d %s%s:\n",
                    ($defect_count == 1) ? 'was' : 'were',
                    $defect_count,
                    $type,
                    ($defect_count == 1) ? '' : 's'
                )
            );
            $this->write('</div>');

            $i = 1;
            foreach ($defects as $defect) {
                $this->printDefect($defect, $i++);
            }
            //................................................

            $this->write('</div>');
        }
    }

    protected function printDefect(TestFailure $defect, $count)
    {
        $this->write('<div class="phpunit-defect">');
        $this->printDefectHeader($defect, $count);
        $this->printDefectTrace($defect);
        $this->printDefectCode($defect);
        $this->write('</div>');
    }

    protected function printDefectHeader(TestFailure $defect, $count)
    {
        $this->write('<div class="phpunit-defect-header">');
        parent::printDefectHeader($defect, $count);
        $this->write('</div>');
    }

    protected function printDefectTrace(TestFailure $defect)
    {
        $this->write('<div class="phpunit-defect-trace">');
        parent::printDefectTrace($defect);
        $this->write('</div>');
    }

    protected function printDefectCode(TestFailure $defect)
    {
        /*
        $this->write('<div class="phpunit-defect-code">');

        $trace = $defect->thrownException()->getTrace();
        $trace_count = count($trace);
        $this->write(dump($trace[3]));

        $this->write('</div>');
        */
    }

    protected function printFooter(TestResult $result)
    {
        $this->write('<div class="phpunit-footer">');
        parent::printFooter($result);
        $this->write('</div>');
    }

    public function flush()
    {
        //parent::flush();
    }

    public function getAutoFlush()
    {
        return false; //parent::getAutoFlush();
    }

    public function incrementalFlush()
    {
        //parent::incrementalFlush();
    }

}