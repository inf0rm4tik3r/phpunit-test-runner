<?php

declare(strict_types = 1);

namespace Lukaspotthast\PHPUnitTestRunner;

use DOMDocument;

/**
 * Class PHPUnit_Helper
 * @package Lukaspotthast\DSV\Tests
 */
final class PHPUnit_Test_Runner
{

    /**
     * Do not forget to call nl2br on the output when putting it in HTML code.
     * @param string $tests_directory
     * @param string $suffixes
     * @return string
     */
    public static function run_get_result(string $tests_directory, string $suffixes = 'Test.php'): string
    {
        $script = __DIR__.'/PHPUnit_Test_Runner_Script.php';
        $args = [
            '"'.$tests_directory.'"',
            '"'.$suffixes.'"'
        ];
        $args_string = implode(' ', $args);
        $cmd = 'php '.$script.' '.$args_string;

        $child = popen($cmd, 'r');
        $result = stream_get_contents($child);
        pclose($child);

        $result = self::remove_leading_p($result);
        return $result;
    }

    private static function remove_leading_p(string $html): string
    {
        $dom = new DOMDocument();
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $html);

        $html = $dom->childNodes->item(2);
        $body = $html->childNodes->item(0);
        $body->removeChild($body->childNodes->item(0));

        // TODO: save without html, body and added xml tags.
        return $dom->saveHTML();
    }

}