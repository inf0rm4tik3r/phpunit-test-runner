<?php

declare(strict_types = 1);

use PHPUnit\Framework\Exception as PHPUnit_Exception;
use PHPUnit\TextUI\ResultPrinter;
use PHPUnit\TextUI\TestRunner;
use Lukaspotthast\PHPUnitTestRunner\HTML_Result_Printer;

// Require call to the autoload file of this package.
//require __DIR__.'/../vendor/autoload.php';

// Require call to the autoload file of the application which installed this package.
/** @noinspection PhpIncludeInspection */
require __DIR__.'/../../../autoload.php';


function run_phpunit(string $tests_directory, string $suffixes): void
{
    // Testrunner and printer.
    $phpunit = new TestRunner();
    $result_printer = new HTML_Result_Printer(null, true, ResultPrinter::COLOR_NEVER);
    $phpunit->setPrinter($result_printer);

    $test_suite = $phpunit->getTest($tests_directory, '', $suffixes);
    $phpunit->dorun($test_suite, ['debug' => true, /*, 'coveragePHP' => true*/]);
}

try
{
    run_phpunit($argv[1], $argv[2]);
}
catch ( PHPUnit_Exception $e )
{
    return $e->getMessage();
}